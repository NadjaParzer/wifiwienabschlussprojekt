import {
  Avatar,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core'
import MoneyIcon from '@material-ui/icons/Money'

const TotalDownloads = ({ totalDownloads }) => (
  <Card
    sx={{ height: '100%' }}
    style={{ backgroundColor: '#1A3567' }}
  >
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}
      >
        <Grid item>
          <Typography
            color="#ffd504"
            gutterBottom
            variant="h6"
          >
            TOTAL DOWNLOADS 2021
          </Typography>
          <Typography
            color="white"
            variant="h3"
          >
            {totalDownloads}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: '#8D9AB3',
              color: 'black',
              height: 56,
              width: 56
            }}
          >
            <MoneyIcon />
          </Avatar>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
)

export default TotalDownloads
