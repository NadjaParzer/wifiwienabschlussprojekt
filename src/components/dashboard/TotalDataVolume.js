import {
  Avatar,
  Card,
  CardContent,
  Grid,
  Typography
} from '@material-ui/core'
import Storage from '@material-ui/icons/Storage'

const TotalDataVolume = ({ totalDataVolume }) => (
  <Card style={{ backgroundColor: '#1A3567' }}>
    <CardContent>
      <Grid
        container
        spacing={3}
        sx={{ justifyContent: 'space-between' }}
      >
        <Grid item>
          <Typography
            color="#ffd504"
            gutterBottom
            variant="h6"
          >
            TOTAL DATA VOLUME
          </Typography>
          <Typography
            color="white"
            variant="h3"
          >
            {`${totalDataVolume} TB`}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar
            sx={{
              backgroundColor: '#8D9AB3',
              color: 'black',
              height: 56,
              width: 56
            }}
          >
            <Storage />
          </Avatar>
        </Grid>
      </Grid>
    </CardContent>
  </Card>
)

export default TotalDataVolume
