import { Doughnut } from 'react-chartjs-2'
import React from 'react'
import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Typography,
  colors,
  useTheme
} from '@material-ui/core'

const DistributionByCountrys = ({ countries, ...rest }) => {
  const theme = useTheme()

  const diagramcolors = [{ color: '#00264d' }, { color: '#064481' }, { color: '#ffd504' }, { color: '#24bced' },
  { color: '#282c2f' }, { color: '#f50057' }, { color: '#01786F' }, { color: '#576675' }];

  diagramcolors.length = countries.length

  const countriesForDiagram = diagramcolors.map((obj, index) => {
    const res = countries[index]
    res.color = obj.color
    return res
  })

  // Creating the new array with property 'color'
  const data = {
    datasets: [
      {
        data: countriesForDiagram.map((c) => c.value),
        backgroundColor: countriesForDiagram.map((c) => c.color),
        borderWidth: 2,
        borderColor: colors.common.white,
        hoverBorderColor: colors.common.white
      }
    ],
  };

  const options = {
    animation: false,
    cutoutPercentage: 80,
    layout: { padding: 0 },
    legend: {
      display: true,
      labels: {
        usePointStyle: true,
      },
      title: {
        display: true
      },
    },
    maintainAspectRatio: false,
    responsive: true,
    tooltips: {
      backgroundColor: theme.palette.background.paper,
      bodyFontColor: theme.palette.text.secondary,
      borderColor: theme.palette.divider,
      borderWidth: 1,
      enabled: true,
      footerFontColor: theme.palette.text.secondary,
      intersect: false,
      mode: 'index',
      titleFontColor: theme.palette.text.primary
    }
  };

  return (
    <Card {...rest} >
      <CardHeader title="Verteilung nach Ländern" />
      <Divider />
      <CardContent>
        <Box
          sx={{
            height: 300,
            position: 'relative'
          }}
        >
          <Doughnut
            data={data}
            options={options}
          />
        </Box>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'center',
            pt: 2
          }}
        >
          {countriesForDiagram.map(({
            color,
            title,
            value
          }) => (
            <Box
              key={title}
              sx={{
                p: 1,
                textAlign: 'center'
              }}
            >
              <Typography
                color={color}
                variant="body1"
              >
                {title}
              </Typography>
              <Typography
                style={{ color }}
                variant="h2"
              >
                {value}
                %
              </Typography>
            </Box>
          ))}
        </Box>
      </CardContent>
    </Card>
  )
}

export default React.memo(DistributionByCountrys)
