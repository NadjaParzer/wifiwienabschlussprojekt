import {
  Box,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon
} from '@material-ui/core'
import { Search as SearchIcon } from 'react-feather'
import { useState } from 'react'

const ListToolbar = ({
  searchItem, searchFailed, placeHolder}) => {
  const [inputValue, setInputValue] = useState('')

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      console.log('error', searchFailed)
      searchItem(inputValue)
    }
  }

  return (
    <Box>
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'flex-end'
        }}
      >
      </Box>
      <Box sx={{ mt: 3 }}>
        <Card>
          <CardContent>
            <Box sx={{ maxWidth: 500 }}>
              <TextField
                id="outlined-error-helper-text"
                helperText={searchFailed ? 'Incorrect entry.' : ' '}
                error={searchFailed}
                value={inputValue}
                onKeyPress={(e) => handleKeyPress(e)}
                onChange={(e) => setInputValue(e.currentTarget.value)}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SvgIcon
                        fontSize="small"
                        color="action"
                      >
                        <SearchIcon />
                      </SvgIcon>
                    </InputAdornment>
                  )
                }}
                placeholder={placeHolder}
                variant="outlined"
              />
            </Box>
          </CardContent>
        </Card>
      </Box>
    </Box>
  )
}

export default ListToolbar
