import { useEffect } from 'react'
import { Link as RouterLink, useLocation } from 'react-router-dom'
import {
  Avatar,
  Box,
  Divider,
  Drawer,
  Hidden,
  List,
  Typography
} from '@material-ui/core'
import {
  BarChart2 as BarChartIcon,
  List as ListIcon,
  User as UserIcon,
  Users as UsersIcon,
  Sliders as SlidersIcon,
  Settings,
} from 'react-feather'
import getInitials from 'src/utils/getInitials'
import NavItem from './NavItem'
import { useSelector } from 'react-redux'

const items = [
  {
    href: '/app/dashboard',
    icon: BarChartIcon,
    title: 'Dashboard'
  },
  {
    href: '/app/customers',
    icon: UsersIcon,
    title: 'Kunden'
  },
  {
    href: '/app/products',
    icon: ListIcon,
    title: 'ISO'
  },
  {
    href: '/app/agents',
    icon: SlidersIcon,
    title: 'Agents Steuerung'
  },

  {
    href: '/app/users',
    icon: Settings,
    title: 'All users'
  },
  {
    href: '/app/account',
    icon: UserIcon,
    title: 'Account'
  },
]

const DashboardSidebar = ({ onMobileClose, openMobile }) => {
  const location = useLocation()
  const firstName = useSelector(state => state.userReducer.firstname)
  const lastName = useSelector(state => state.userReducer.lastname)
  const jobTitle = useSelector(state => state.userReducer.position)
  const avatar = useSelector(state => state.userReducer.avatar)
  const roles = useSelector(state => state.userReducer.roles);

  let itemsForRender
  if(!roles.includes("ADMIN")) {
    itemsForRender = items.filter(item => (item.title !== 'All users' && item.title !== 'Agents Steuerung') )
  } else {
    itemsForRender = items
  } 

  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose()
    }
  }, [location.pathname])

  const content = (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
      }}
    >
      <Box
        sx={{
          alignItems: 'center',
          display: 'flex',
          flexDirection: 'column',
          p: 2
        }}
      >
        <Avatar
          component={RouterLink}
          src={`http://localhost:7005/uploads/${avatar}`}
          sx={{
            cursor: 'pointer',
            width: 64,
            height: 64
          }}
          to="/app/account"
          style={{ color: '#f50057' }}
        >
          {getInitials(`${firstName} ${lastName}`)}
        </Avatar>
        <Typography
          color="textPrimary"
          variant="h5"
        >
          {`${firstName} ${lastName}`}
        </Typography>
        <Typography
          color="textSecondary"
          variant="body2"
        >
          {jobTitle}
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        <List>
          {itemsForRender.map((item) => (
            <NavItem
              href={item.href}
              key={item.title}
              title={item.title}
              icon={item.icon}
            />
          ))}
        </List>
      </Box>
    </Box>
  )

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
          PaperProps={{
            sx: {
              width: 256
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden lgDown>
        <Drawer
          anchor="left"
          open
          variant="persistent"
          PaperProps={{
            sx: {
              width: 256,
              top: 64,
              height: 'calc(100% - 64px)'
            }
          }}
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  )
}

// DashboardSidebar.propTypes = {
//   onMobileClose: PropTypes.func,
//   openMobile: PropTypes.bool
// };

// DashboardSidebar.defaultProps = {
//   onMobileClose: () => { },
//   openMobile: false
// };

export default DashboardSidebar
