import { useEffect, useState } from 'react'
import PerfectScrollbar from 'react-perfect-scrollbar'
import {
  Box,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography
} from '@material-ui/core'
import { NavLink } from 'react-router-dom'
import Sorting from '../Sorting'
import { useNavigate } from 'react-router'
import { useSelector } from 'react-redux'
import axios from 'axios'

const CustomerListResults = ({ searchedCustomer }) => {
  const [limit, setLimit] = useState(10)
  const [page, setPage] = useState(0)
  const [customers, setCustomers] = useState([])
  const [count, setCount] = useState(0)
  const [sortLands, setLandSorting] = useState('none')
  const [sortCustomers, setCustomerSorting] = useState('none')
  const [sortDownloads, setDownloadsSorting] = useState('none')

  const navigate = useNavigate()
  const isLogin = useSelector(state => state.loginReducer.islogin)
  const token = useSelector(state => state.loginReducer.apitoken)

  useEffect(() => {
    if (!isLogin) {
      navigate('/login', { replace: true })
    } else {
      axios.get(`http://localhost:7005/app/customers?page=${page + 1}&number=${limit}&sortLands=${sortLands}&sortCustomers=${sortCustomers}&sortDownloads=${sortDownloads}`,
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
          }
        }).then(res => {
          console.log(res)
          setCustomers(res.data.customers)
          setCount(res.data.totalCustomers)
        })
    }
  }, [page, limit, sortLands, sortCustomers, sortDownloads, isLogin])

  // Page rerendering after searching customer by name
  if (searchedCustomer) {
    useEffect(() => {
      setCustomers(searchedCustomer)
    }, [searchedCustomer])
  }

  const handleLimitChange = (event) => {
    setLimit(event.target.value)
  }

  const handlePageChange = (event, newPage) => {
    console.log('newPage', newPage)
    setPage(newPage)
  }

  const handleLandSorting = (sortFactor) => {
    setLandSorting(sortFactor)
    setDownloadsSorting('none')
    setCustomerSorting('none')
  }

  const handleDownloadSorting = (sortFactor) => {
    setLandSorting('none')
    setDownloadsSorting(sortFactor)
    setCustomerSorting('none')
  }

  const handleCustomerSorting = (sortFactor) => {
    setLandSorting('none')
    setDownloadsSorting('none')
    setCustomerSorting(sortFactor)
  }
  return (
    <Card >
      <PerfectScrollbar>
        <Box sx={{ minWidth: 1050 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell style={{ minWidth: '400px' }}>
                  Land
                  <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortLands} callback={handleLandSorting} /></span>
                </TableCell>
                <TableCell>
                  Firmenname
                </TableCell>
                <TableCell style={{ textAlign: 'center' }}>
                  Kundennummer
                  <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortCustomers} callback={handleCustomerSorting} /></span>
                </TableCell>
                <TableCell style={{ textAlign: 'center' }}>
                  Letzte IP
                </TableCell>
                <TableCell style={{ textAlign: 'center' }}>
                  Downloads pro Monat
                  <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortDownloads} callback={handleDownloadSorting} /></span>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers.slice(0, limit).map((customer) => (
                <TableRow
                  hover
                  key={customer.id}
                >
                  <TableCell style={{ minWidth: '400px' }}>
                    <Box
                      sx={{
                        alignItems: 'center',
                        display: 'flex'
                      }}
                    >
                      <Typography
                        color="textPrimary"
                        variant="body1"
                      >
                        {customer.address.country}
                      </Typography>
                    </Box>
                  </TableCell>
                  <TableCell>
                    <NavLink style={{ color: '#24bced', textDecoration: 'underline' }} to={{ pathname: `/app/test/${customer.id}` }} state={customer}>
                      {customer.customerName}
                    </NavLink>
                  </TableCell>
                  <TableCell style={{ textAlign: 'center' }}>
                    <NavLink style={{ color: '#24bced', textDecoration: 'underline' }} to={{ pathname: `/app/test/${customer.id}` }} state={customer}>
                      {customer.customerNumber}
                    </NavLink>
                  </TableCell>
                  <TableCell style={{ textAlign: 'center' }}>
                    {customer.lastIP}
                  </TableCell>
                  <TableCell style={{ textAlign: 'center' }}>
                    {customer.downloadsMonth}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={count}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </Card>
  )
}

export default CustomerListResults
