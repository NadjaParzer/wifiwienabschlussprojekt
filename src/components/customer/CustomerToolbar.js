import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  Paper,
  Typography,
  Grid
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import PropTypes from 'prop-types';

const CustomerToolbar = ({ customer, ...rest }) => (
  <Box {...rest}>
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'flex-end'
      }}
      mb={3}
    >
      {/* <Button
        color="primary"
        variant="contained"
      >
        export
      </Button> */}
    </Box>
    <Paper elevation={3} square>
      <Card>
        <CardContent>
          <Grid container spacing={3} alignItems="center">
            <Grid item xl={6} xs={12}>
              <Typography pb={2} variant="h5">
                {customer.customerName}
                {' #'}
                {customer.customerNumber}
              </Typography>
              <Typography>
                {customer.address.country}
                {', '}
                {customer.address.city}
              </Typography>
              <Typography>
                {customer.address.street}
              </Typography>
            </Grid>
            <Grid item xl={6} xs={12}>
              <Typography pb={2} variant="h5">
                {customer.contactPerson}
              </Typography>
              <Typography>
                {customer.email}
              </Typography>
              <Typography>
                {customer.phone}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Paper>
    <Box sx={{ mt: 3 }}>
      <Card>
        <CardContent>
          <Box sx={{ maxWidth: 500 }}>
            <TextField
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SvgIcon
                      fontSize="small"
                      color="action"
                    >
                      <SearchIcon />
                    </SvgIcon>
                  </InputAdornment>
                )
              }}
              placeholder="Search ISO"
              variant="outlined"
            />
          </Box>
        </CardContent>
      </Card>
    </Box>
  </Box>
);

CustomerToolbar.propTypes = {
  customer: PropTypes.any
};

export default CustomerToolbar;
