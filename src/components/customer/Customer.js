import { Helmet } from 'react-helmet'
import CustomerToolbar from 'src/components/customer/CustomerToolbar'
import { useLocation } from 'react-router'
import { useState } from 'react'
import PerfectScrollbar from 'react-perfect-scrollbar'
import {
  Box,
  Container,
  Card,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
} from '@material-ui/core'
import moment from 'moment'

const Customer = () => {
  const [limit, setLimit] = useState(10)
  const [page, setPage] = useState(0)
  const location = useLocation()
  const customer = location.state

  const handleLimitChange = (event) => {
    setLimit(event.target.value)
  }

  const handlePageChange = (event, newPage) => {
    setPage(newPage)
  }

  return (
    <>
      <Helmet>
        <title>
          {customer.customerName}
        </title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth={false}>
          <CustomerToolbar customer={customer} />
          <Box sx={{ pt: 3 }}>
            <Card>
              <PerfectScrollbar>
                <Box sx={{ minWidth: 1050 }}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>
                          ISO ID
                        </TableCell>
                        <TableCell>
                          AGENT
                        </TableCell>
                        <TableCell>
                          Download START
                        </TableCell>
                        <TableCell>
                          Download END
                        </TableCell>
                        <TableCell>
                          Dauer
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {customer.isos.map((iso) => (
                        <TableRow
                          hover
                          key={iso.isoID}
                        >
                          <TableCell>
                            {iso.isoID}
                          </TableCell>
                          <TableCell>
                            {iso.agent}
                          </TableCell>
                          <TableCell>
                          {moment(iso.downloadStart).format('DD.MM.YYYY, HH:mm Z')}
                          </TableCell>
                          <TableCell>
                          {moment(iso.downloadEnd).format('DD.MM.YYYY, HH:mm Z')}
                          </TableCell>
                          <TableCell>
                            {iso.dauer}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </Box>
              </PerfectScrollbar>
              <TablePagination
                component="div"
                count={customer.isos.length}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleLimitChange}
                page={page}
                rowsPerPage={limit}
                rowsPerPageOptions={[5, 10, 25]}
              />
            </Card>
          </Box>
        </Container>
      </Box>
    </>
  )
}

export default Customer
