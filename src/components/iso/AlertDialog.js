import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import axios from 'axios'
import { useSelector } from 'react-redux'

export default function AlertDialog({
  isDisabled, isoID, updateRelease, ...rest }) {
  const [open, setOpen] = React.useState(false)
  const token = useSelector(state => state.loginReducer.apitoken)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleRelease = () => {
    axios.post(`http://localhost:7005/app/release`, {isoID}, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
    }).then(res => {
handleClose()
    updateRelease(true)
    })
  }
 if(isDisabled) {
   return <></>
 } else {
   return (
    <>
      <Button variant="outlined" color="primary" disabled={isDisabled} size="small" onClick={handleClickOpen}>
        Freigeben
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        {...rest}
      >
        <DialogTitle id="alert-dialog-title">
        Bitte bestätigen
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          Möchten Sie die Änderungen für Image #{isoID} speichern?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Abbrechen
          </Button>
          <Button onClick={handleRelease} variant="contained" size="small" color="primary" autoFocus>
            Freigeben
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
 }
  
}
