import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import CachedIcon from '@material-ui/icons/Cached'
import DoneIcon from '@material-ui/icons/Done'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline'
import DoneAllIcon from '@material-ui/icons/DoneAll'

const IsoFreigabe = ({ status }) => {
  let component
  switch (status) {
    case 'Herunterladen':
      component = <ArrowDownwardIcon fontSize="large" color="primary" />
      break
    case 'Verteilung':
      component = <CachedIcon fontSize="large" color="secondary" />
      break;
    case 'Nicht Freigegeben':
      component = <DoneIcon fontSize="large" style={{ color: 'green' }} />
      break
    case 'Freigegeben':
      component = <DoneAllIcon style={{ color: 'green' }} />
      break
    default:
      component = <ErrorOutlineIcon fontSize="large" style={{ color: 'red' }} />
      break
  }

  return (
    component
  )
}

export default IsoFreigabe
