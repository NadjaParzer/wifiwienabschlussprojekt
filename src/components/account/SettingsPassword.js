import { useState } from 'react'
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
  Typography
} from '@material-ui/core'
import { useSelector } from 'react-redux'
import { updatePasswordTC } from 'src/reducers/userReducer'
import { useDispatch } from 'react-redux'

const SettingsPassword = (props) => {
  const username = props.user.mail
  const [err, setError] = useState(false)
  const dispatch = useDispatch()
  const [span, setSpan] = useState('none')
  const [values, setValues] = useState({
    password: '',
    confirm: ''
  })

  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    })
  }

  const updatePassword = () => {
    if(values.password !== values.confirm) {
      setError(true)
    } else {
      setError(false)
    dispatch(updatePasswordTC(props.token, username, values.confirm))
    setValues({password:'', confirm: ''})
    setSpan('inline')
  }
}

  return (
    <form {...props}>
      <Card>
        <CardHeader
          subheader="Update password"
          title="Password"
        />
        <Divider />
        <CardContent>
          <TextField
            fullWidth
            label="Password"
            margin="normal"
            name="password"
            onChange={handleChange}
            type="password"
            value={values.password}
            variant="outlined"
          />
          <TextField
            fullWidth
            label="Confirm password"
            error={err}
            margin="normal"
            name="confirm"
            onChange={handleChange}
            type="password"
            value={values.confirm}
            variant="outlined"
          />
        </CardContent>
        <Divider />
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            p: 2
          }}
        >
          <Typography style={{ marginRight: '10px', color: '#24bced' ,display: `${span}`}} >Password ist geändert!</Typography>
          <Button
            color="primary"
            variant="contained"
            onClick={updatePassword}
          >
            Update
          </Button>
        </Box>
      </Card>
    </form>
  )
}

export default SettingsPassword
