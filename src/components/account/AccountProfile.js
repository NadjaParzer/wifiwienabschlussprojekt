import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  Divider,
  Typography
} from '@material-ui/core'
import axios from 'axios'
import { useEffect, useState } from 'react'
import getInitials from 'src/utils/getInitials'
import { useDispatch } from 'react-redux'
import { updateAvatarTC } from 'src/reducers/userReducer'

const AccountProfile = (props) => {
  const username = props.user.mail
  const [avatar, setAvatar] = useState(props.user.avatar)
  const dispatch = useDispatch()

  //Rerendering Avatar Image after uploading new photo 
  useEffect(() => {
    setAvatar(props.user.avatar)
  }, [props.user.avatar])

  // separate the uploaded file 
  const uploadPicture = (event) => {
    const files = event.target.files
    uploadOnServer(files[0])
  }

  // Server request
  const uploadOnServer = (file) => {
    const formData = new FormData()
    formData.append("picture", file)
    formData.append('username', username)
    axios.post(`http://localhost:7005/app/auth/uploadPicture`, formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer ' + props.token,
        }
      }
    ).then(res => {
      console.log('Upload picture response: ',res)
      // Redux-Thunk technology
      dispatch(updateAvatarTC(props.token, username))
    }
    )
  }

  return (
    <Card >
      <CardContent>
        <Box
          sx={{
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'column'
          }}
        >
          <Avatar
            src={`http://localhost:7005/uploads/${avatar}`}
            sx={{
              height: 100,
              width: 100
            }}
            style={{ color: '#f50057' }}
          >
            {/* if user don't have any photo,will be shown the first letters of his name and family name */}
            {getInitials(`${props.user.firstname} ${props.user.lastname}`)}
          </Avatar>

          <Typography
            color="textPrimary"
            gutterBottom
            variant="h3"
          >
            {props.user.firstname}
            {' '}
            {props.user.lastname}
          </Typography>
          <Typography
            color="textSecondary"
            variant="body1"
          >
            {props.user.position ? props.user.position : ''}
          </Typography>
          <Typography
            color="textSecondary"
            variant="body1"
          >
            {props.user.city && props.user.country ? `${props.user.city} ${props.user.country}` : ''}
          </Typography>
        </Box>
      </CardContent>
      <Divider />
      <CardActions>
        <label htmlFor="upload-photo">
          <input value="" onChange={(event) => uploadPicture(event)} id="upload-photo" type="file" style={{ display: 'none' }} />
          <Button color="primary" variant="text" fullWidth component="span">
            Upload picture
          </Button>
        </label>
      </CardActions>
    </Card>
  )
}

export default AccountProfile
