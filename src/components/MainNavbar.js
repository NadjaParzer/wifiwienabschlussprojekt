import { Link as RouterLink } from 'react-router-dom'
import { AppBar, Toolbar, Typography } from '@material-ui/core'

const MainNavbar = (props) => (
  <AppBar
    elevation={0}
    {...props}
  >
    <Toolbar sx={{ height: 64 }}>
      <RouterLink to="/">
        <Typography variant="h2" style={{ color: '#ffd504' }}>
           LOGO
        </Typography>
      </RouterLink>
    </Toolbar>
  </AppBar>
);

export default MainNavbar;
