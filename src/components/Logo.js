const Logo = (props) => (
  <img alt="Logo" src="/static/images/logo.svg" {...props} />);
// const Logo = () => (
//   <div>
//     <h1 style={{ color: '#ffd504' }}>LOGO</h1>
//   </div>
// );

export default Logo;
