import { Helmet } from 'react-helmet'
import {
  Box,
  Container,
  Grid
} from '@material-ui/core'
import AccountProfile from 'src/components/account/AccountProfile'
import AccountProfileDetails from 'src/components/account/AccountProfileDetails'
import SettingsPassword from 'src/components/account/SettingsPassword'
import { useNavigate } from 'react-router'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'

const Account = () => {
  const navigate = useNavigate()
  const user = useSelector(state => state.userReducer)
  const isLogin = useSelector(state => state.loginReducer.islogin)
  const token = useSelector(state => state.loginReducer.apitoken)

  // Checking if user is logged in, if not => redirect to the LOGIN Page
  useEffect(() => {
    !isLogin && navigate('/login', { replace: true })
  }, [isLogin])

  return (
    <>
      <Helmet>
        <title>Account</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth="lg">
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              lg={4}
              md={6}
              xs={12}
            >
              <AccountProfile token={token} user={user} />
            </Grid>
            <Grid
              item
              lg={8}
              md={6}
              xs={12}
            >
              <AccountProfileDetails user={user} />
            </Grid>
            <Grid
              item
              lg={4}
              md={6}
              xs={12}
            >
              {' '}
            </Grid>
            <Grid
              item
              lg={8}
              md={6}
              xs={12}
            >
              <SettingsPassword user={user} token={token}/>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  )
}
export default Account
