import { Helmet } from 'react-helmet'
import {
  Box,
  Card,
  CardContent,
  Container,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core'
import PerfectScrollbar from 'react-perfect-scrollbar'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router'
import axios from 'axios'

const ManageUsers = () => {
  const [users, setUsers] = useState([])
  const navigate = useNavigate()
  const isLogin = useSelector(state => state.loginReducer.islogin)
  const token = useSelector(state => state.loginReducer.apitoken)
  const roles = useSelector(state => state.userReducer.roles)

  useEffect(() => {
    if (!isLogin) {
      navigate('/login', { replace: true })
    } else {
      axios.get(`http://localhost:7005/app/auth/users`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        }
      }).then(
        (res) => {
          setUsers(res.data)
        }
      )
    }
  }, [isLogin])

  if (!roles.includes('ADMIN')) {
    return <Card style={{ marginTop: '50px', textAlign: 'center' }}>
      <CardContent>
        <Typography gutterBottom variant="h4" component="div">
          Sorry, you don't have access to this page
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Only Administrator has permissions to open this page
        </Typography>
      </CardContent>
    </Card>
  }

  return (
    <>
      <Helmet>
        <title>USERS</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >

        <Container maxWidth={false}>
          <Box sx={{ pt: 3 }}>
            <Card>
              <PerfectScrollbar>
                <Box sx={{ minWidth: 1050 }}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>
                          Login
                        </TableCell>
                        <TableCell>
                          Vorname
                        </TableCell>
                        <TableCell>
                          Nachname
                        </TableCell>
                        <TableCell>
                          Position
                        </TableCell>
                        <TableCell>
                          Phone
                        </TableCell>
                        <TableCell>
                          Location
                        </TableCell>
                        <TableCell>
                          Rights
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {users.map((user) => {
                        return (
                          <TableRow
                            hover
                            key={user.username}
                          >
                            <TableCell>
                              {user.username}
                            </TableCell>
                            <TableCell>
                              {user.firstname}
                            </TableCell>
                            <TableCell>
                              {user.lastname}
                            </TableCell>
                            <TableCell>
                              {user.position}
                            </TableCell>
                            <TableCell>
                              {user.phone}
                            </TableCell>
                            <TableCell>
                              {user.country}
                              {' '}
                              {user.city}
                            </TableCell>
                            <TableCell>
                              {user.roles[0]}
                            </TableCell>
                          </TableRow>
                        )
                      })}
                    </TableBody>
                  </Table>
                </Box>
              </PerfectScrollbar>
            </Card>
          </Box>
        </Container>
      </Box>
    </>
  )
}

export default ManageUsers
