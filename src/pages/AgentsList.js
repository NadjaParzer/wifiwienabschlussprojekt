import { Helmet } from 'react-helmet'
import {
  Box,
  Card,
  CardContent,
  Container,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core'
import PerfectScrollbar from 'react-perfect-scrollbar'
import DoneIcon from '@material-ui/icons/Done'
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline'
import { NavLink } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router'
import axios from 'axios'

const Agents = () => {
  const [agents, setAgents] = useState([])
  const navigate = useNavigate()
  const isLogin = useSelector(state => state.loginReducer.islogin)
  const token = useSelector(state => state.loginReducer.apitoken)
  const roles = useSelector(state => state.userReducer.roles)

  useEffect(()=> {
    if (!isLogin) {
      navigate('/login', { replace: true })
    } else {
      axios.get(`http://localhost:7005/app/agents`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
      }}).then(
      (res) => {
        setAgents(res.data.agents);
      }
    )
   }
  }, [isLogin])

  if(!roles.includes('ADMIN')) {
    return <Card style={{marginTop: '50px', textAlign: 'center'}}>
      <CardContent>
        <Typography gutterBottom variant="h4" component="div">
          Sorry, you don't have access to this page
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Only Administrator has permissions to open this page
        </Typography>
      </CardContent>
    </Card>
  }

  return (
    <>
      <Helmet>
        <title>AGENTS</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth={false}>
          <Box sx={{ pt: 3 }}>
            <Card>
              <PerfectScrollbar>
                <Box sx={{ minWidth: 1050 }}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>
                          Agent
                        </TableCell>
                        <TableCell>
                          Volume diesen Monat
                        </TableCell>
                        <TableCell>
                          Volume letzte 24h
                        </TableCell>
                        <TableCell>
                          Downloads diesen Monat
                        </TableCell>
                        <TableCell>
                          Downloads letzte 24h
                        </TableCell>
                        <TableCell>
                          Status
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {agents.map((agent) => {
                        let icon
                        if (agent.status === 'active') {
                          icon = <DoneIcon style={{ color: 'green' }} />
                        } else {
                          icon = <RemoveCircleOutlineIcon style={{ color: 'red' }} />
                        }
                        return (
                          <TableRow
                            hover
                            key={agent.agentID}
                          >
                            <TableCell>
                              <NavLink style={{ color: '#24bced', textDecoration: 'underline' }} to={{ pathname: `/app/agent/${agent.agentID}` }} state={agent}>
                                {agent.agentNummer}
                              </NavLink>
                            </TableCell>
                            <TableCell>
                              {agent.monthVolume}
                              {' GB'}
                            </TableCell>
                            <TableCell>
                              {agent.lastDayVolume}
                              {' GB'}
                            </TableCell>
                            <TableCell>
                              {agent.monthDownloads}
                            </TableCell>
                            <TableCell>
                              {agent.lastDayDownloads}
                            </TableCell>
                            <TableCell>
                              {icon}
                            </TableCell>
                          </TableRow>
                        )
                      })}
                    </TableBody>
                  </Table>
                </Box>
              </PerfectScrollbar>
            </Card>
          </Box>
        </Container>
      </Box>
    </>
  )
}
export default Agents
