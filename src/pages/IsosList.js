import { Helmet } from 'react-helmet'
import {
  Box,
  Card,
  Container,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
} from '@material-ui/core'
import { useEffect, useState } from 'react'
import PerfectScrollbar from 'react-perfect-scrollbar'
import IsoFreigabe from 'src/components/iso/IsoFreigabe'
import AlertDialog from 'src/components/iso/AlertDialog'
import ListToolbar from 'src/components/ListToolbar'
import Sorting from 'src/components/Sorting'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router'
import axios from 'axios'
import moment from 'moment'

const IsosList = () => {
  const [limit, setLimit] = useState(10)
  const [page, setPage] = useState(0)
  const [isos, setIsos] = useState([])
  const [count, setCount] = useState(0)
  const [update, updateRelease] = useState(false)
  const [searchedISO, setSearchedISO] = useState([])
  const [searchFailed, setClass] = useState(false)
  const token = useSelector(state => state.loginReducer.apitoken)
  const navigate = useNavigate()
  const isLogin = useSelector(state => state.loginReducer.islogin)
  const roles = useSelector(state => state.userReducer.roles)

  // //////////////////////////////////////////////
  // Table sorting
  const [sortID, setIDSorting] = useState('none')
  const [sortDate, setDateSorting] = useState('none')
  const [sortSize, setSizeSorting] = useState('none')
  const [sortDownloads, setDownloadsSorting] = useState('none')
  const [sortTotalDuration, setTotalDurSorting] = useState('none')
  const [sortMediumDuration, setMeduimDurSorting] = useState('none')
  const [sortReleaseDate, setReleaseDateSorting] = useState('none')
  const [sortStatus, setStatusSorting] = useState('none')

  const syncSorting = (func) => {
    const funcArray = [setIDSorting, setDateSorting, setSizeSorting, setDownloadsSorting, setTotalDurSorting, setMeduimDurSorting, setReleaseDateSorting, setStatusSorting]
    const sortArray = funcArray.filter((el) => el !== func)
    return sortArray.forEach((el) => el('none'))
  }
  const handleIDSorting = (sortFactor) => {
    setIDSorting(sortFactor)
    syncSorting(setIDSorting)
  }
  const handleDateSorting = (sortFactor) => {
    setDateSorting(sortFactor)
    syncSorting(setDateSorting)
  }
  const handleSizeSorting = (sortFactor) => {
    setSizeSorting(sortFactor)
    syncSorting(setSizeSorting)
  }
  const handleDownloadsSorting = (sortFactor) => {
    setDownloadsSorting(sortFactor)
    syncSorting(setDownloadsSorting)
  }
  const handleTotalDurSorting = (sortFactor) => {
    setTotalDurSorting(sortFactor)
    syncSorting(setTotalDurSorting)
  }
  const handleMediumDurSorting = (sortFactor) => {
    setMeduimDurSorting(sortFactor)
    syncSorting(setMeduimDurSorting)
  }
  const handleReleaseDateSorting = (sortFactor) => {
    setReleaseDateSorting(sortFactor)
    syncSorting(setReleaseDateSorting)
  }
  const handleStatusSorting = (sortFactor) => {
    setStatusSorting(sortFactor)
    syncSorting(setStatusSorting)
  }

  useEffect(() => {
    if (!isLogin) {
      !isLogin && navigate('/login', { replace: true });
    } else {
      axios.get(`http://localhost:7005/app/isos?page=${page + 1}&number=${limit}&sortID=${sortID}&sortDate=${sortDate}&sortSize=${sortSize}&sortDownloads=${sortDownloads}&sortTotalDuration=${sortTotalDuration}&sortMediumDuration=${sortMediumDuration}&sortReleaseDate=${sortReleaseDate}&sortStatus=${sortStatus}`,
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
          }
        })
        .then((res) => {
          setIsos(res.data.isos);
          setCount(res.data.totalIsos);
        });
    }
  }, [page, limit, update, sortID, sortDate, sortSize, sortDownloads, sortTotalDuration, sortMediumDuration, sortReleaseDate, sortStatus, isLogin]);

  if (searchedISO) {
    useEffect(() => {
      setIsos(searchedISO);
    }, [searchedISO]);
  }

  const searchISO = (isoName) => {
    axios.get(`http://localhost:7005/app/isos/search?isoName=${isoName}`,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        }
      }).then(
        (res) => {
          if (res.data.resultCode === 1) {
            setClass(false)
            setSearchedISO(res.data.data)
          } else {
            setClass(true)
            setSearchedISO([])
          }
        }
      )
  }

  const handleLimitChange = (event) => {
    setLimit(event.target.value)
  }

  const handlePageChange = (event, newPage) => {
    setPage(newPage)
  }

  return (
    <>
      <Helmet>
        <title>ISOs</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth={false}>
          <ListToolbar searchItem={searchISO} searchFailed={searchFailed} placeHolder="Search ISO" />
          <Box sx={{ pt: 3 }}>
            <Card>
              <PerfectScrollbar>
                <Box sx={{ minWidth: 1050 }}>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>
                          ISO ID
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortID} callback={handleIDSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Datum
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortDate} callback={handleDateSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Größe (GB)
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortSize} callback={handleSizeSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Downloads
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortDownloads} callback={handleDownloadsSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Total Dauer
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortTotalDuration} callback={handleTotalDurSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Durchschn. Dauer
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortMediumDuration} callback={handleMediumDurSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Freigabe
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortReleaseDate} callback={handleReleaseDateSorting} /></span>
                        </TableCell>
                        <TableCell>
                          Status
                          <span style={{ cursor: 'pointer', marginLeft: '5px' }}><Sorting sortFactor={sortStatus} callback={handleStatusSorting} /></span>
                        </TableCell>
                        <TableCell>
                          {' '}
                        </TableCell>
                        <TableCell>
                          {' '}
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {isos.slice(0, limit).map((iso) => (
                        <TableRow
                          hover
                          key={iso.id}
                        >
                          <TableCell>
                            {iso.id}
                          </TableCell>
                          <TableCell>
                            {moment(iso.createdAt).format('DD.MM.YYYY, HH:mm Z')}
                          </TableCell>
                          <TableCell>
                            {iso.size}
                          </TableCell>
                          <TableCell>
                            {iso.downloads}
                          </TableCell>
                          <TableCell>
                            {iso.totalDuration}
                          </TableCell>
                          <TableCell>
                            {iso.averageDuration}
                          </TableCell>
                          <TableCell>
                            {moment(iso.dateOfPublish).format('DD.MM.YYYY, HH:mm Z')} 
                          </TableCell>
                          <TableCell>
                            {iso.status}
                          </TableCell>
                          <TableCell>
                            <IsoFreigabe status={iso.status} />
                          </TableCell>
                          <TableCell>
                            <AlertDialog updateRelease={updateRelease} isDisabled={iso.status !== 'Nicht Freigegeben' || !roles.includes('ADMIN')} isoID={iso.id} />
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </Box>
              </PerfectScrollbar>
              <TablePagination
                component="div"
                count={count}
                onPageChange={handlePageChange}
                onRowsPerPageChange={handleLimitChange}
                page={page}
                rowsPerPage={limit}
                rowsPerPageOptions={[5, 10, 25]}
              />
            </Card>
          </Box>
        </Container>
      </Box>
    </>
  )
}

export default IsosList
