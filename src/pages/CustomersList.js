import { Helmet } from 'react-helmet'
import { Box, Container } from '@material-ui/core'
import CustomerListResults from 'src/components/customer/CustomerListResults'
import ListToolbar from 'src/components/ListToolbar'
import { useState } from 'react'
import { useSelector } from 'react-redux'
import axios from 'axios'

const CustomerList = () => {
  const [searchedCustomer, setSearchedCustomer] = useState([])
  const [searchFailed, setClass] = useState(false)
  const token = useSelector(state => state.loginReducer.apitoken)

  const searchCustomer = (customerName) => {
    axios.get(`http://localhost:7005/app/customers/search?customerName=${customerName}`,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        }
      }).then(
        (res) => {
          if (res.data.resultCode === 1) {
            setClass(false)
            setSearchedCustomer(res.data.data)
          } else {

            setClass(true)
            setSearchedCustomer([])
          }
        }
      )
  }

  return (
    <>
      <Helmet>
        <title>Kunden</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth={false}>
          <ListToolbar placeHolder="Search customer" searchFailed={searchFailed} searchItem={searchCustomer} />
          <Box sx={{ pt: 3 }}>
            <CustomerListResults searchedCustomer={searchedCustomer} />
          </Box>
        </Container>
      </Box>
    </>
  )
}

export default CustomerList
