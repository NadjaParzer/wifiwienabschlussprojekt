import {
  Box,
  Container,
  Grid
} from '@material-ui/core'
import TotalDownloads from 'src/components/dashboard//TotalDownloads'
import TotalCustomers from 'src/components/dashboard//TotalCustomers'
import TotalDataVolume from 'src/components/dashboard//TotalDataVolume'
import DistributionByCountrys from 'src/components/dashboard/DistributionByCountrys'
import { Helmet } from 'react-helmet'
import { useEffect, useState } from 'react'
import NumberOfDownloads from 'src/components/dashboard/NumberOfDownloads'
import { useNavigate } from 'react-router'
import { useSelector } from 'react-redux'
import axios from 'axios'


const Dashboard = () => {
  const [countries, setCountries] = useState([])
  const [dashboardData, setDashboard] = useState({})
  const [isos, setIsos] = useState([])
  const navigate = useNavigate()
  const isLogin = useSelector(state => state.loginReducer.islogin)
  const token = useSelector(state => state.loginReducer.apitoken)

  useEffect(() => {
    if (!isLogin) {
      navigate('/login', { replace: true })
    } else {
      axios.get(`http://localhost:7005/app/dashboard`,
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
          }
        }).then(res => {
          console.log('DASHBOARD', res.data)
          setCountries(res.data.countryDistribution)
          setIsos(res.data.isosDownloads)
          setDashboard(res.data)
        })
    }
  }, [isLogin])

  return (
    <>
      <Helmet>
        <title>Dashboard</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 3
        }}
      >
        <Container maxWidth={false}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              lg={4}
              sm={6}
              xl={4}
              xs={12}
            >
              <TotalDownloads totalDownloads={dashboardData.totalDownloads} style={{ backgroundColor: '#1A3567' }} />
            </Grid>
            <Grid
              item
              lg={4}
              sm={6}
              xl={4}
              xs={12}
            >
              <TotalCustomers totalCustomers={dashboardData.totalCustomers} style={{ backgroundColor: '#1A3567' }} />
            </Grid>
            <Grid
              item
              lg={4}
              sm={12}
              xl={4}
              xs={12}
            >
              <TotalDataVolume
                totalDataVolume={dashboardData.totalDataVolume}
                style={{ backgroundColor: '#1A3567' }}
                sx={{ height: '100%' }}
              />
            </Grid>
            <Grid
              item
              lg={8}
              md={12}
              xl={9}
              xs={12}
              mt={7}
            >
              <NumberOfDownloads year={dashboardData.year} isosDownloads={isos} />
            </Grid>
            <Grid
              item
              lg={4}
              md={6}
              xl={3}
              xs={12}
              mt={7}
            >
              <DistributionByCountrys countries={countries} sx={{ height: '100%' }} />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  )
}

export default Dashboard
