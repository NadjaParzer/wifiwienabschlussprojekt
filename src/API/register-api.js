const { default: axios } = require('axios');

const instance = axios.create({
  baseURL: 'http://localhost:7005/app/'
});

const registerAPI = {
  registration(username, password, firstname, lastname ){
    return  instance.post('auth/registration', {username, password, firstname, lastname});
   },
   login(username, password){
    return  instance.post('auth/login', {username, password});
   },
}

export default registerAPI;