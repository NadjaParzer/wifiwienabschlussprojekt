import registerAPI from "src/API/register-api"
import { setUserData } from "./userReducer"

export const GET_TOKEN_SUCCESS = 'POST_LOGING_SUCCESS'
export const GET_TOKEN_FAILURE = 'POST_LOGING_FAILURE'
export const GET_TOKEN_LOGOUT = 'GET_TOKEN_LOGOUT'

export const getTokenBegin = () => ({
  type: GET_TOKEN_BEGIN
})

export const getTokenSuccess = (tokendata) => ({
  type: GET_TOKEN_SUCCESS,
  payload: { tokendata }
})

export const getTokenLogout = (islogin) => ({
  type: GET_TOKEN_LOGOUT,
  payload: { islogin }
})

export const getTokenFailure = (error) => ({
  type: GET_TOKEN_FAILURE,
  payload: { error }
})

// function handleErrors(response) {
//   if (!response.ok) {
//     throw Error(response.statusText);
//   }
//   return response;
// }

export function login(username, password) {
  return (dispatch) => {
    registerAPI.login(username, password)
      .then(res => {
        console.log(res.data.status)
        if(res.data.status !== 400) {
          dispatch(getTokenSuccess(res.data.token))
          dispatch(setUserData(res.data.firstname, res.data.lastname, res.data.country, res.data.city, res.data.phone, res.data.mail, res.data.position, res.data.roles, res.data.avatar))
        } else {
          (dispatch(getTokenFailure(res.data.message)))
        }
      })
  }
}

const initialState = {
  apitoken: null,
  islogin: false,
  error: '',
}

export function loginReducer(state = initialState, action) {

  switch (action.type) {
    case GET_TOKEN_LOGOUT:
      return {
        ...state,
        apitoken: null,
        islogin: false,
        error: null
      }
    case GET_TOKEN_SUCCESS:
      return {
        ...state,
        apitoken: action.payload.tokendata,
        loading: false,
        error: null,
        islogin: true,
      }
    case GET_TOKEN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    default:
      return state
  }
}
