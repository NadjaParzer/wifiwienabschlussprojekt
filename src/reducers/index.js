import { combineReducers } from 'redux'
import { loginReducer } from './loginReducer'
import { userReducer } from './userReducer'

const rootReducers = combineReducers({
  loginReducer,
  userReducer
})
export default rootReducers
