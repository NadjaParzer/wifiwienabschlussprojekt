import axios from "axios"
export const SET_USER_DATA = 'SET_USER_DATA'
export const UPDATE_AVATAR = 'UPDATE_AVATAR'
export const UPDATE_PASSWORD = 'UPDATE_PASSWORD'

export const setUserData = (firstname, lastname, country, city, phone, mail, position, roles, avatar) => ({
  type: SET_USER_DATA, firstname, lastname, mail, position,country, city, phone, roles, avatar
})

export const updateAvatar = (avatar) => ({
  type: UPDATE_AVATAR, avatar
})

export const updatePassword = (password) => ({
  type: UPDATE_PASSWORD, password
})

export function updateUserDataTC(firstname, lastname, mail, country, city, position, phone) {
  return (dispatch, getState) => {
    const username = getState().userReducer.mail
    const body= {username, firstname, lastname, mail, country, city, position, phone }
    axios.post('http://localhost:7005/app/auth/update', body).then(res => {
      console.log('update', res.data)
      dispatch(setUserData(res.data.firstname, res.data.lastname, res.data.country, res.data.city, res.data.phone, res.data.mail, res.data.position, res.data.roles, res.data.avatar))
       })
  }
}

export function updateAvatarTC(token, username) {
  return (dispatch) => {
    axios.get(`http://localhost:7005/app/auth/getPicture?username=${username}`, {
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
    }).then(res => {
      console.log(res)
      dispatch(updateAvatar(res.data.avatar))
    })
  }
}

export function updatePasswordTC(token, username, password) {
  return(dispatch) => {
    let body = {username: username, password: password}
    axios.post('http://localhost:7005/app/auth/updatePassword', body, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
      }
    }).then(res => {
      dispatch(updatePassword(password))
    })
  }
}

const initialState = {
  mail: '',
  phone: '',
  password: '',
  firstname: '',
  lastname: '',
  avatar: '',
  country: '',
  city: '',
  position: '',
  roles: []
}

export function userReducer(state = initialState, action) {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        firstname: action.firstname,
        lastname: action.lastname,
        mail: action.mail,
        position: action.position,
        phone: action.phone,
        country: action.country,
        city: action.city,
        roles: action.roles,
        avatar: action.avatar
      }
      case UPDATE_AVATAR:
        return {
          ...state,
          avatar: action.avatar
        }
      case UPDATE_PASSWORD:
        return {
          ...state,
          password: action.password
        }
    default:
      return state
  }
}
